
Bio3D GeoStaS Domain Finder
===========================

Bio3D-GeoStaS is an [R](http://www.r-project.org/) package for the identification of geometrically stable domains in biomolecules.

It is a package of the larger [Bio3D](https://bitbucket.org/Grantlab/bio3d) family containing utilities for the analysis of protein structure, sequence and trajectory data. It is currently distributed as platform independent source code under the [GPL version 2 license](http://www.gnu.org/copyleft/gpl.html).

Installation
------------

The development version of Bio3d-geostas is available from BitBucket:

``` r
install.packages("devtools")
devtools::install_bitbucket("Grantlab/bio3d-geostas")
```

Make sure you have installed [Bio3D](https://bitbucket.org/Grantlab/bio3d) prior to installing Bio3D-geostas:

``` r
install.packages("bio3d", dependencies=TRUE)
```

Basic usage
-----------

``` r
library(bio3d.geostas)

pdb <- read.pdb("1d1d", multi=TRUE)
#>   Note: Accessing on-line PDB file

## Find domains and write PDB
gs  <- geostas(pdb, fit=TRUE)
#>   .. 220 'calpha' atoms selected
#>   .. 'xyz' coordinate data with 20 frames 
#>   .. 'fit=TRUE': running function 'core.find'
#>   .. coordinates are superimposed to core region
#>   .. calculating atomic movement similarity matrix ('amsm.xyz()') 
#>   .. dimensions of AMSM are 220x220
#>   .. clustering AMSM using 'kmeans' 
#>   .. converting indices to match input 'pdb' object 
#>      (additional attribute 'atomgrps' generated)

## Plot a atomic movement similarity matrix
plot(gs, contour=FALSE)
```

![](man/figures/README-syntax_demo1-1.png)

``` r
# fit pdb 
xyz <- fit.xyz(pdb$xyz[1, ], pdb$xyz, fixed.inds=gs$fit.inds, mobile.inds=gs$fit.inds)

# write pdb with domain assignment
write.pdb(pdb, chain=gs$atomgrps, xyz=xyz)
```

![](man/figures/README-syntax_demo-1d1d.png)

Development
-----------

For information on current and future development of Bio3D, see the Bio3D project on BitBucket. Details of recent package changes and reported issues can also be found on BitBucket.

Citing Bio3D
------------

-   The Bio3D packages for structural bioinformatics <br> Grant, Skjærven, Yao, (2020) *Protein Science* <br> ( [Abstract](https://onlinelibrary.wiley.com/doi/abs/10.1002/pro.3923) | [PubMed]() | [PDF]() )

-   Integrating protein structural dynamics and evolutionary analysis with Bio3D. <br> Skjærven, Yao, Scarabelli, Grant, (2014) *BMC Bioinformatics* **15**, 399 <br> ( [Abstract](http://www.biomedcentral.com/1471-2105/15/399/abstract) | [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/25491031) | [PDF](http://www.biomedcentral.com/content/pdf/s12859-014-0399-6.pdf) )

-   Bio3D: An R package for the comparative analysis of protein structures. <br> Grant, Rodrigues, ElSawy, McCammon, Caves, (2006) *Bioinformatics* **22**, 2695-2696 <br> ( [Abstract](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/22/21/2695) | [PubMed](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=16940322&dopt=Abstract) | [PDF](http://bioinformatics.oxfordjournals.org/content/22/21/2695.full.pdf) )
